% Quand vous aurez réussi à importer ce fichier Prolog, toutes les règles
% seront prises en compte dans votre terminal Prolog.
% Pour chaque règle il y a un exemple que vous pouvez directement
% copier/coller dans votre terminal Prolog pour tester.

% -----------------------------------------------------------------%

% Construction des naturels.
% Fait de base.
nat([zero]).
% Règle récursive. Si la tête n'est pas zero, on vérifie que la tête est "s"
% et on rappelle la règle sur le reste.
nat([H|T]) :- H = "s", nat(T).

% Exemple:
% nat([zero]). -> true
% nat(["s", "s", "zero"]). -> true

% L'addition
add([zero], Y, Y).
add(["s"|X] , Y, Res) :- append(["s"], Y, Y1), add(X, Y1, Res).

% Exemple:

% add(["s", zero], ["s", zero], Res). retourne Res = ["s", "s", zero]

% Greater than: X > Y
gt(["s", zero], [zero]).
gt(["s", X], ["s", Y]) :- gt(X,Y).

% -----------------------------------------------------------------%

% Construction de sa propre structure liste avec des integers
list(empty).
list(Head, empty) :- integer(Head).
list(Head1, list(Head2, Tail)) :- integer(Head1), list(Head2, Tail).

% Exemple:
% list(5,list(3, list(42, empty))).

% Est-ce que ma liste contient un élément particulier ?
contains(X, list(X, _)).
contains(X, list(_, T)) :- contains(X, T).

% Exemple:
% contains(3, list(5,list(3, empty))).

max(list(H, empty), H).
max(list(H1, T), H1) :- max(T, Res), H1 >= Res.
max(list(H1, T), Res) :- max(T, Res), H1 < Res.

% Exemple:
% max(list(1, list(42, list(4, empty))), R). -> Res = 42
